var valoresPlano = [0, 0, 0]
,valoresTotalPlanos = [0, 0, 0]
,valoresTreino = [0, 0, 0];

// Plano Visibility
paramVisibility = 0;

//Plano Control
paramControl = 0;

//Plano Resilience
paramResilience = 0;

//Variaveis valores total
var valorTotalTreino = 0
,valorTotalPlanos = 0
,valorTotalParame = 0;

//valores total de cada plano
var statePrice = 0;

$(document).ready(function(){

    //Quando carrega a página faz o request dos valores mensais
    requestValores();

    //Caso mude o pagamento para ano ou mês
    $('.op-pagamento').on('click', function(){
        if($(this).hasClass('anual')){
            $(this).removeClass('anual');
            $(this).addClass('mensal');
            $('.price-ano').removeClass('active');
            $('.price-mes').addClass('active');
            statePrice = 0;
        }else{
            $(this).removeClass('mensal');
            $(this).addClass('anual');
            $('.price-mes').removeClass('active');
            $('.price-ano').addClass('active');
            statePrice = 1;
        }
        requestValores();
        calculaTotal();
    });

    // Se mudar o check adm calcula o valor
    $('#check_adm').on('change', function(){
        if(this.checked){
            let ativos = $('.qnt-ativos');
            let total = 0;
            for(let i = 0; i < ativos.length; i++){
                if(ativos[i].value != ''){
                    total += Number(ativos[i].value);
                }
            }
            total *= 1.78;
            $('table.total').append('<tr id="price-adm"><td>Adm. e  Operação Absolute (12 parcelas) no cartão de:</td><td>'+total.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})+'</td>');
        }else{
            $('#price-adm').remove();
        }
    });

    $('.qnt-ativos').on('input', function(){
        calculaTotal();      
        $('.content-select').show('fast');
    });

    $('#sel_tecnicos').on('change', function(){
        calculaTecnicos();
    });
});

//Calcula o preço total dos serviços
function calculaTotal(){
    somaPlano();
    valorParametrizacao();
    calculaTecnicos();
}

//calcula o preço do treinamento
function calculaTecnicos (){
    let ativos = $('.qnt-ativos');
    let vPlano = 0;
    let valorTreino = 0;
    for(let i = 0; i < ativos.length; i++){
        if($(ativos[i]).val() != '' && Number($(ativos[i]).val()) > 0){
            if(vPlano < Number($(ativos[i]).attr('data-plano'))){
                vPlano = Number($(ativos[i]).attr('data-plano'));
            }
        }
    }

    if(vPlano > 0)
        valorTreino = (Number($('#sel_tecnicos').val()) * valoresTreino[vPlano - 1]) / 2;
    

    $('#price-total-treino').html(valorTreino.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}));
}

//Soma o valor do plano
function somaPlano(){
    for(let iPlano = 1; iPlano <= 3; iPlano++){
        let qntAtivos = $('.qnt-ativos[data-plano="'+ iPlano.toString()+'"]');
        let total = 0;
        for(let i = 0; i < qntAtivos.length; i++){
            total += Number($(qntAtivos[i]).val()) * valoresPlano[iPlano - 1];   
        }
        valoresTotalPlanos[iPlano - 1] = total;
    }

    total = 0;

    for(let i = 0; i < valoresTotalPlanos.length; i++){
        total += valoresTotalPlanos[i];
    }
    valorTotalPlanos = total;

    $('#price-total-parcelas').html(valorTotalPlanos.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}));

}

function valorParametrizacao(){
    let ativos = $('.qnt-ativos');
    let plano = 0;
    for(var i = 0; i < ativos.length; i++){
        if(ativos[i].value != '' && Number(ativos[i].value) > 0){
            let planoAtual = Number($(ativos[i]).attr('data-plano'));
            if(planoAtual > plano){
                plano = planoAtual;
                if(planoAtual == 1){
                    valorTotalParame = paramVisibility;
                }else if(planoAtual == 2){
                    valorTotalParame = paramControl;                        
                }else if(planoAtual == 3){
                    valorTotalParame = paramResilience;
                }else{
                    valorTotalParame = 0;
                }
            }
        }
    }
    $('#price-total-param').html(valorTotalParame.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}));
}

function requestValores(){
    $.ajax({
        url:"nnovax-ajax.php",
        type:"post",
        dataType: "json",
        data: {
            'state_price':  statePrice
            ,'educacao': 0
            ,'action': 'request_price'
        },
        beforeSend: function(){
            // Antes de carregar
        },
        success: function (result){
            
            for(let i = 0; i < 3; i++){
                valoresPlano[i] = result.valoresPlano[i];
                valoresTreino[i] = result.valoresTreino[i];
            }

            // Plano Visibility
            paramVisibility = result.paramVisibility;

            //Plano Control
            paramControl = result.paramControl;
            
            //Plano Resilience 
            paramResilience = result.paramResilience;
            calculaTotal();
        },
        error: function (result) {
            console.log(result);
            console.log('Erro ao carregar valores.');
        }
    });
}
