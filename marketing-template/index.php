<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Marketig Template</title>

    <link rel="stylesheet" href="./css/reset.css" />
    <link rel="stylesheet" href="./css/bootstrap.min.css" />
    <link rel="stylesheet" href="./css/main.css" />
</head>
<body>
    
    <header class="topo">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <figure class="logo">
                        <img src="./images/logo.png" alt="Logo" />
                    </figure>

                    <ul class="contatos">
                        <li>
                            <a href="#">(11) 9999-9999</a>
                        </li>
                    </ul>

                    <nav class="content-menu">
                        <ul class="menu">
                            <li>
                                <a href="#">Home</a>
                            </li>
                            <li>
                                <a href="#">About</a>
                            </li>
                            <li>
                                <a href="#">Services</a>
                            </li>
                            <li>
                                <a href="#">Pages</a>
                            </li>
                            <li>
                                <a href="#">Shop</a>
                            </li>
                            <li>
                                <a href="#">News</a>
                            </li>
                            <li>
                                <a href="#">Get a Quote</a>
                            </li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </header>

    <section class="banner" style="background-image: url(./images/bg-banner.jpg)">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="text-banner">
                        <h2>Make some noise with our creative team</h2>
                        <h1>Digital <br> Agency</h1>
                    </div>
                    
                </div>
                <div class="col-lg-6">
                    <figure class="image-banner">
                        <img src="./images/image-banner.png" alt="">
                    </figure>
                </div>
            </div>
        </div>
    </section>
    
    <section class="twitter">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <a class="twitter-timeline" href="https://twitter.com/mano_p_erro404?ref_src=twsrc%5Etfw">Tweets by mano_p_erro404</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
                </div>
                <div class="col-md-6">
                    <a class="twitter-timeline" href="https://twitter.com/Viih_Santtoos?ref_src=twsrc%5Etfw">Tweets by Viih_Santtoos</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
                </div>
            </div>
        </div>
    </section>

</body>
</html>