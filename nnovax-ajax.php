<?php


//valores Mensais
$valoresM = array(
    'valoresPlano' => array(
        8.81
        ,14.27
        ,21.30
    )
    ,'valoresTreino' => array(
        470.81
        ,713.93
        ,993.26
    )
    ,'paramVisibility' => 701.8
    ,'paramControl' => 979.32
    ,'paramResilience' => 1298.16
);


//valores Anuais
$valoresA = array(
    'valoresPlano' => array(
        92.74
        ,154.29
        ,233.42
    )
    ,'valoresTreino' => array(
        259.19
        ,470.81
        ,713.93
    )
    ,'paramVisibility' => 460.25
    ,'paramControl' => 701.80
    ,'paramResilience' => 979.32
);

//valres para educação
$valoresEM = array(
    'visibilityM' => 8.81
    ,'controlM' => 14.27
    ,'resilenceM' => 21.30
    ,'treinVisibilityM' => 470.81
    ,'paramVisibilityM' => 701.8
    ,'treinControlM' => 713.93
    ,'paramControlM' => 979.32
    ,'treinResilienceM' => 993.26
    ,'paramResilienceM' => 1298.16
);

$valoresEA = array(
    'visibility' => 8.81
    ,'control' => 14.27
    ,'resilence' => 21.30
    ,'treinVisibility' => 470.81
    ,'paramVisibility' => 701.8
    ,'treinControl' => 713.93
    ,'paramControl' => 979.32
    ,'treinResilience' => 993.26
    ,'paramResilience' => 1298.16
);


if(isset($_POST['action'])){

    if($_POST['state_price'] == '1'){
        echo json_encode(($_POST['educacao'] == '1') ? $valoresEM : $valoresA);
    }else if($_POST['state_price'] == '0'){
        echo json_encode(($_POST['educacao'] == '1') ? $valoresEM : $valoresM);
    }

}
