<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NNOVAX</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="./style.css">
</head>
<body>
    
    <section class="banner">
        <figure class="item-banner">
            <img src="./banner-nnovax.jpg" alt="banner" />
        </figure>
    </section>

    <section class="detalhes-abolute">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form action="" class="form-absolute">
                        <div class="content-op-pagamento">
                            <p class="pagamento-mensal">Mensal</p>
                            <span class="op-pagamento mensal"></span>
                            <p class="pagamento-anual">Anual</p>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="content-plano">
                                    <h2 class="title-plano">Absolute Visibility</h2>
                                    <div class="content-price">
                                        <span class="price-plano" id="price-visibility">
                                            <span class="translate-price price-mes active">R$ 8,81 p/dispositivo <br /> mensal</span>
                                            <span class="translate-price price-ano">R$ 92,74 p/dispositivo <br /> anual</span>
                                        </span>
                                    </div><!-- price plano -->
                                    <p style="min-height: 24px"></p>
                                    <ul class="lista-plano">
                                        <li><i class="fas fa-check"></i> <span>Inventário de Hardwware;</span></li>
                                        <li><i class="fas fa-check"></i> <span>Inventário de Software;</span></li>
                                        <li><i class="fas fa-check"></i> <span>Localização e Histórico dos equipamentos;</span></li>
                                        <li><i class="fas fa-check"></i> <span>Relatórios sobre a saúde de suas aplicações;</span></li>
                                        <li><i class="fas fa-check"></i> <span>Alertas automatizados;</span></li>
                                        <li><i class="fas fa-check"></i> <span>Tecnologia Persistente(r) em BIOS.</span></li>
                                    </ul>
                                    <div class="content-opcoes-plano">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="txt_notebooks">Seus Notebooks</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="number" min="0" name="notebooks" id="txt_notebooks" class="qnt-ativos" data-plano="1" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="txt_desktops">Seus Desktops</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="number" min="0" name="desktops" id="txt_desktops" class="qnt-ativos" data-plano="1" />
                                            </div>
                                        </div>
                                    </div><!-- content opcoes plano -->
                                </div><!-- content-plano  -->
                            </div><!-- col md 4 -->
                            <div class="col-md-4">
                                <div class="content-plano">
                                    <h2 class="title-plano">Absolute Control</h2>
                                    <div class="content-price">
                                        <span class="price-plano" id="price-visibility">
                                            <span class="translate-price price-mes active">R$ 14,27 p/dispotivo <br /> mensal</span>
                                            <span class="translate-price price-ano">R$ 154,29 p/dispositivo <br /> anual</span>
                                        </span>
                                    </div><!-- price plano -->
                                    <p class="text-mais">Tudo do Visibility e...</p>
                                    <ul class="lista-plano">    
                                        <li><i class="fas fa-check"></i> <span>Bloqueio e desbloqueio remoto;</span></li>
                                        <li><i class="fas fa-check"></i> <span>Exclusão seletiva ou total de dados remotamente;</span></li>
                                        <li><i class="fas fa-check"></i> <span>Cercas geográficas virtuais contra usos não autorizados.</span></li>
                                    </ul><!-- lista plano -->
                                    <div class="content-opcoes-plano">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="txt_notebooks">Seus Notebooks</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="number" min="0" name="notebooks" id="txt_notebooks" class="qnt-ativos" data-plano="2" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="txt_desktops">Seus Desktops</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="number" min="0" name="desktops" id="txt_desktops" class="qnt-ativos" data-plano="2" />
                                            </div>
                                        </div>
                                    </div><!-- content opcoes plano -->
                                </div><!-- content planno -->
                            </div><!-- col md 4 -->
                            <div class="col-md-4">
                                <div class="content-plano">
                                    <h2 class="title-plano">Absolute Resilience</h2>
                                    <div class="content-price">
                                        <span class="price-plano" id="price-visibility">
                                            <span class="translate-price price-mes active">R$ 21,30 p/dispositivo <br /> mensal</span>
                                            <span class="translate-price price-ano">R$ 233,42 p/dispositivo <br /> anual</span>
                                        </span>
                                    </div><!-- price plano -->
                                    <p class="text-mais">Tudo do Control e...</p>
                                    <ul class="lista-plano">
                                        <li><i class="fas fa-check"></i> <span>Auto-cura automática de aplicações terceiras;</span></li>
                                        <li><i class="fas fa-check"></i> <span>Remotamente escanei arquivos com dados sensíveis (LGPDP / GDPR);</span></li>
                                        <li><i class="fas fa-check"></i> <span>Execute remotamente scripts PowerShell ou Bash em qualquer equipamento;</span></li>
                                        <li><i class="fas fa-check"></i> <span>Execute análise de riscos em equipamentos suspeitos de riscos;</span></li>
                                        <li><i class="fas fa-check"></i> <span>Investigação e recuperação física de equipamentos Roubados!</span></li>
                                    </ul><!-- lista plano -->
                                    <div class="content-opcoes-plano">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="txt_notebooks">Seus Notebooks</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="number" min="0" name="notebooks" id="txt_notebooks" class="qnt-ativos" data-plano="3" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="txt_desktops">Seus Desktops</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="number" min="0" name="desktops" id="txt_desktops" class="qnt-ativos" data-plano="3" />
                                            </div>
                                        </div>
                                    </div><!-- content opcoes plano -->
                                </div><!-- content plano -->
                            </div><!-- col md 4 -->
                        </div><!-- row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="content-select">
                                    <label for="sel_tecnicos">Técnicos para treinamento</label>
                                    <select name="" id="sel_tecnicos">
                                        <?php for($i = 2; $i <= 20; $i += 2): ?>
                                        <option value="<?= $i ?>"><?= $i ?></option>
                                        <?php endfor; ?>
                                    </select>              
                                </div>
                            </div>
                        </div>
                        <fieldset>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="align-right">
                                        <label for="check_estudante">Se seu negócio é educação, temos valores ainda melhores para vocês. Marque Aqui e confira acima.</label>
                                        <input type="checkbox" name="" id="check_estudante" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="text-adm">Está sem pessoal para fazer a Administração e Operação Absolute? Sem problemas, podemos te ajudar com isso também.</p>
                                    <input type="checkbox" name="adm" id="check_adm">
                                    <label for="check_adm">Incluir Adm. Operação Absolute</label>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="title-total">Total</h4>
                                    <table class="total">
                                        <tr>
                                            <td>Valor treinamento em 3x no cartão:</td>
                                            <td id="price-total-treino">0</td>
                                        </tr>
                                        <tr>
                                            <td>Valor Parametrização em 3x no cartão:</td>
                                            <td id="price-total-param">0</td>
                                        </tr>
                                        <tr>
                                            <td>12 parcelas Mensais no cartão:</td>
                                            <td id="price-total-parcelas">0</td>
                                        </tr>
                                    </table>
                                    <button type="submit" class="button-comprar">Comprar Agora</button>
                                    <div style="margin-bottom: 30px;s"></div>
                                </div> 
                            </div>
                        </fieldset>
                    </form><!-- form-abolute -->
                    <p>
                        Tem mais de 1.000 equipamentos para proteger - fale conosco para descontos especiais. Clique Aqui.
                    </p>
                    <p>
                        (*) Não é garantia de recuperação. Se precisar de Garantia 100% clique aqui.
                    </p>
                </div><!-- col md 12 -->
            </div><!-- row -->
        </div><!-- container -->
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="./main.js"></script>
</body>
</html>