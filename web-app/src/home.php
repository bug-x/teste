<html>
    <head>
        <title>LAYOUT | TESTE</title>
        <?php include_once 'includes/head.php';?>
    </head>
    <body lang="pt-br">
        <header>
            <div class="container">
                <div id="topo">
                    <p>Take your business to the next level.</p>
                    <h2>Build your own website today</h2>
                    <a href="#" class="btn">Sing Up</a>
                    <div id="background-orbit"></div>
                </div>
            </div>
        </header>
        <div id="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <img src="img/logo.png" id="logo" />
                    </div>
                    <div class="col-md-9">
                        <div id="menu">
                            <a href="#" class="btn">Login</a>
                            <ul>
                                <li><a href="home" data-action="load-page">Home</a></li>
                                <li><a href="produtos" data-action="load-page">About</a></li>
                                <li><a href="#">Domains</a></li>
                                <li><a href="#">Hosting</a></li>
                                <li><a href="#">Pages</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#"></a></li>
                            </ul>
                        </div><div style="clear: both;"></div>
                    </div>
                </div>
            </div>
        </div>
        <section id="services">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2>Why Choose Our Services</h2>
                        <p>Nulla euismod lorem nisi, a tempor diam fringilla ut. Nunc sed consectetur mauris. Nullam ac sem elit. Vestibulum porta odio lacinia mauris.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="grid">
                            <img src="#" />
                            <div class="detalhes">
                                <h3>Data Analysis</h3>
                                <p>Lorem ipsum dolor sit amet, placeat necessitatibus adipisicing elit.</p>
                                <a class="btn">More Details</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="grid">
                            <img src="#" />
                            <div class="detalhes">
                                <h3>Easy Settings</h3>
                                <p>Lorem ipsum dolor sit amet, placeat necessitatibus adipisicing elit.</p>
                                <a class="btn">Quick Tour</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="grid">
                            <img src="#" />
                            <div class="detalhes">
                                <h3>Free upgrade</h3>
                                <p>Lorem ipsum dolor sit amet, placeat necessitatibus adipisicing elit.</p>
                                <a class="btn">Sing Up</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <section id="dominios">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="titulo">
                            <h2>Register Domain & <span>Get 10% Off</span> Today</h2>
                            <p>Let’s find your domain</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="pesquisa">
                            <form action="#">
                                <input type="text" placeholder="Digite a pesquisa" />
                                <input type="submit" value="SEARCH"/>
                            </form>
                        </div>
                        <div id="precos">
                            <div>
                                <img src="img/com.jpg" /><p>$3.99/year</p>
                            </div>
                            <div>
                                <img src="img/net.jpg" /><p>$3.99/year</p>
                            </div>
                            <div>
                                <img src="img/org.jpg" /><p>$3.99/year</p>
                            </div>
                            <div>
                                <img src="img/au.jpg" /><p>$3.99/year</p>
                            </div>
                            <div>
                                <img src="img/eu.jpg" /><p>$3.99/year</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="planos">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Our <span>Pricing Plan</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="tablist">
                            <ul>
                                <li><a href="#" class="active">Web Hosting</a></li>
                                <li><a href="#">Cloud Hosting</a></li>
                                <li><a href="#">Shared Hosting</a></li>
                                <li><a href="#">Reseller Hosting</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="plano">
                            <div class="grid">
                                <span>Basic</span>
                                <div>
                                    <div class="circulo">
                                        <p class="valor">$29</p>
                                        <p class="duracao">Per month</p>
                                    </div>
                                </div>
                                <ul>
                                    <li>Storage — 5 GB</li>
                                    <li>Bandwidth ( Traffic ) — 15 GB</li>
                                    <li>Domain name — Free!</li>
                                    <li>Ram — 128 MB</li>
                                    <li>Subdomains — 1 GB</li>
                                </ul>
                                <a href="#" class="btn">Start Plan</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="plano">
                            <div class="grid">
                                <span>Statndard</span>
                                <div>
                                    <div class="circulo">
                                        <p class="valor">$49</p>
                                        <p class="duracao">Per month</p>
                                    </div>
                                </div>
                                <ul>
                                    <li>Storage — 5 GB</li>
                                    <li>Bandwidth ( Traffic ) — 15 GB</li>
                                    <li>Domain name — Free!</li>
                                    <li>Ram — 128 MB</li>
                                    <li>Subdomains — 1 GB</li>
                                </ul>
                                <a href="#" class="btn">Start Plan</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="plano">
                            <div class="grid">
                                <span>Premiium</span>
                                <div>
                                    <div class="circulo">
                                        <p class="valor">$79</p>
                                        <p class="duracao">Per month</p>
                                    </div>
                                </div>
                                <ul>
                                    <li>Storage — 5 GB</li>
                                    <li>Bandwidth ( Traffic ) — 15 GB</li>
                                    <li>Domain name — Free!</li>
                                    <li>Ram — 128 MB</li>
                                    <li>Subdomains — 1 GB</li>
                                </ul>
                                <a href="#" class="btn">Start Plan</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <section id="features">
            <div class="container">
                <div class="bloco-features">
                    <div class="row">
                        <div class="col-md-5">
                            <img src="img/img-1.jpg"/>
                        </div>
                        <div class="col-md-7">
                            <h2>99.9% Uptime</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore reprehenderit laborum, ab exercitationem sunt aliquam illo! Aliquid labore animi, alias ratione ad necessitatibus ipsa perspiciatis. Quae architecto totam, reprehenderit ipsa.</p>
                        </div>
                    </div>
                </div>

                <div class="bloco-features">
                    <div class="row">
                        <div class="col-md-7">
                            <h2>99.9% Uptime</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore reprehenderit laborum, ab exercitationem sunt aliquam illo! Aliquid labore animi, alias ratione ad necessitatibus ipsa perspiciatis. Quae architecto totam, reprehenderit ipsa.</p>
                        </div>
                        <div class="col-md-5">
                            <img src="img/img-2.jpg"/>
                        </div>
                    </div>
                </div>

                <div class="bloco-features">
                    <div class="row">
                        <div class="col-md-5">
                            <img src="img/img-3.jpg"/>
                        </div>
                        <div class="col-md-7">
                            <h2>99.9% Uptime</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore reprehenderit laborum, ab exercitationem sunt aliquam illo! Aliquid labore animi, alias ratione ad necessitatibus ipsa perspiciatis. Quae architecto totam, reprehenderit ipsa.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="clientes">
            <div class="container">
                  <div class="row">
                      <div class="col-md-12">
                        <h2>Client's <span>Testimonials</span></h2>
                    </div>
                  </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="grid">
                            <div class="client-quote">
                                <h3>Quick Support</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore reprehe nderit laborum.</p>
                            </div>
                            <div class="info-client">
                                <img src="img/cliente-img-1.jpg"/>
                                <div class="nome">
                                    <h4>John Olliver</h4>
                                    <span>CTO, Hostgator</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="grid">
                            <div class="client-quote">
                                <h3>Swiss knife</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore reprehe nderit laborum.</p>
                            </div>
                            <div class="info-client">
                                <img src="img/cliente-img-3.jpg"/>
                                <div class="nome">
                                    <h4>Maria Anatoli</h4>
                                    <span>CTO, Hostgator</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="grid">
                            <div class="client-quote">
                                <h3>Top Class Service</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore reprehe nderit laborum.</p>
                            </div>
                            <div class="info-client">
                                <img src="img/cliente-img-2.jpg"/>
                                <div class="nome">
                                    <h4>Lara Zarsson</h4>
                                    <span>CTO, Hostgator</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <a href="#" class="btn">VIEW ALL</a>
                    </div>
                </div>
            </div>
        </section>

        <section id="postagens">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="img-postagem">
                            <img src="img/postagem-img-1.jpg">
                        </div>
                        <div class="grid">
                            <div class="postagem-header">
                                <h3><a href="#">Servers Hacked to Host PoS Malware</a></h3>
                            </div>
                            <div class="postagem-info">
                                <ul>
                                    <li><img src="img/icone-relogio.png" class="icone">23 Nov</li>
                                    <li><img src="img/icone-comentarios.png" class="icone">5 Comments</li>
                                </ul>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consec tetur adipisicing elit. Reiciendis dolo res ducimus pariatur.</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="img-postagem">
                            <img src="img/postagem-img-2.jpg">
                        </div>
                        <div class="grid">
                            <div class="postagem-header">
                                <h3><a href="#">Configure DNS to Enable a Trust networking</a></h3>
                            </div>
                            <div class="postagem-info">
                                <ul>
                                    <li><img src="img/icone-relogio.png" class="icone">23 Nov</li>
                                    <li><img src="img/icone-comentarios.png" class="icone">5 Comments</li>
                                </ul>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consec tetur adipisicing elit. Reiciendis dolo res ducimus pariatur.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="img-postagem">
                            <img src="img/postagem-img-3.jpg">
                        </div>
                        <div class="grid">
                            <div class="postagem-header">
                                <h3><a href="#">Servers Hacked to Host PoS Malware</a></h3>
                            </div>
                            <div class="postagem-info">
                                <ul>
                                    <li><img src="img/icone-relogio.png" class="icone">23 Nov</li>
                                    <li><img src="img/icone-comentarios.png" class="icone">5 Comments</li>
                                </ul>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consec tetur adipisicing elit. Reiciendis dolo res ducimus pariatur.</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="img-postagem">
                            <img src="img/postagem-img-4.jpg">
                        </div>
                        <div class="grid">
                            <div class="postagem-header">
                                <h3><a href="#">Configure DNS to Enable a Trust networking</a></h3>
                            </div>
                            <div class="postagem-info">
                                <ul>
                                    <li><img src="img/icone-relogio.png" class="icone">23 Nov</li>
                                    <li><img src="img/icone-comentarios.png" class="icone">5 Comments</li>
                                </ul>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consec tetur adipisicing elit. Reiciendis dolo res ducimus pariatur.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <a href="#" class="btn">More blogs</a>
                    </div>
                </div>
            </div>
        </section>
        <section id="footer-contato">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Never Hesitate to give us a call </h2>
                        <h3>+123 (4567) 890</h3>
                    </div>
                    <div class="col-md-6">
                        <h2 style="font-weight: bold">Subscribe for monthly Newsletter</h2>
                        <form action="#">
                            <input type="text" placeholder="Your E-mail" />
                            <input type="submit" value="SEARCH"/>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="grid">
                            <img src="img/logo.png">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua.</p>
                            <ul>
                                <li><img src="img/icone-telefone.png" class="icone">+123 (4567) 890 </li>
                                <li><img src="img/icone-email.png" class="icone">info@top-finance.com</li>
                                <li><img src="img/icone-local.png" class="icone">380 St Kilda Road, Melbourne VIC 3004.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div id="links">
                            <h3>Links</h3>
                            <ul>
                                <li><a href="#">Shared Hosting</a></li>
                                <li><a href="#">VPS Hosting</a></li>
                                <li><a href="#">Dedicated Hosting</a></li>
                                <li><a href="#">Windows Hosting</a></li>
                                <li><a href="#">Cloud Hosting</a></li>
                                <li><a href="#">Reseller Hosting</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div id="suport">
                            <h3>Support</h3>
                            <ul>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Submit a Ticket</a></li>
                                <li><a href="#">Visit Knowledge Base</a></li>
                                <li><a href="#">Support System</a></li>
                                <li><a href="#">Refund Policy</a></li>
                                <li><a href="#">Professional Services</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div id="twitter">
                            <h3>Twitter Feed</h3>
                            <div class="twit">
                                <p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit, sed do eius mod tempor incididunt.</p>
                                <div class="profile">
                                    <img src="img/icone-twitter-azul.png" class="icone" /><a href="#">@Mark Wahlberg</a>
                                </div>
                            </div>
                        </div>

                        <div id="twitter">
                            <div class="twit">
                                <p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit, sed do eius mod tempor incididunt.</p>
                                <div class="profile">
                                    <img src="img/icone-twitter-azul.png" class="icone" /><a href="#">@Mark Wahlberg</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>2017 © All Rights Reserved by <a href="#">Themexriver</a></p>
                    </div>
                    <div class="col-md-6">
                        <div id="follow">
                            <ul>
                                <li>Follow us:</li>
                                <li> <a href="#"><img src="img/icone-facebook.png" class="icone"/></a></li>
                                <li> <a href="#"><img src="img/icone-twitter.png" class="icone"/></a></li>
                                <li> <a href="#"><img src="img/icone-google.png" class="icone"/></a></li>
                                <li> <a href="#"><img src="img/icone-linkedin.png" class="icone"/></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>