<?php

class App{
    
    private $dev;
    private $rotas;
    private $host;
    
    public function __construct($d = null){
        if($d){
            $this->dev = $d;
        }
        $this->host = 'http://' . $_SERVER['HTTP_HOST'] . $this->dev;
        $this->rotas = array();
    }
    public function set_rota($rota){
        $this->rotas[] = $rota;
    }
    public function get_rotas(){
        return $this->rotas;
    }
    public function get_host(){
        return $this->host;
    }

    public function get_page($page){

        ob_start();
        include './src/' . $page . '.php';
        $var = ob_get_clean();

        echo $var;


    }

}

if(isset($_POST['action'])){
    $app = new App('/testes/web-app/');
    $app->get_page($_POST['page']);
}
