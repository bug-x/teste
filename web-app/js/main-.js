$(window).bind("load", function() {

});
$(document).ready(function () {
    chamarGaleria = 0;
    $('.imagem a').on('click', function () {
        $('.modal').addClass('active');
        $('.modal .galeria').addClass('active');

        $('.adicionar').on('click', function () {
            $('.aba.images').removeClass('active');
            $('.aba.adicionar').addClass('active');
        });

        $('.voltar').on('click', function () {
            $('.aba.adicionar').removeClass('active');
            $('.aba.images').addClass('active');
        });

        if(chamarGaleria === 0){
            $.ajax({
                url:"?",
                type:"post",
                dataType: "json",
                data: {action: 'chamarGaleria'},
                beforeSend: function(){
                    //
                },
                success: function (result){
                    retorno = '';
                    for(i = 0; i < result.length; i++){
                        retorno += "<div class='item'><img data-id='"+result[i]['id']+"' src='images/"+result[i]['src']+"' /></div>";
                    }
                    $('.modal .images').append(retorno);
                },
                error: function (result) {
                    console.log(result);
                }
            })
            chamarGaleria = 1;
        }
    });

    $('.galeria .fechar-modal').on('click', function () {
        $('.modal').removeClass('active');
        $('.modal .galeria').removeClass('active');
    });



    $('#img').change(function () {
        if(this.files.length > 1) {
            $('label[for="img"]').html(this.files.length + ' itens selecionados');
        } else {
            $('label[for="img"]').html(this.files.item(0).name);
        }
        if(this.files.length > 0) {
            $('#upload_imagem input[type="submit"]').addClass('ativo');
        }
    });

    $('#enviarImagens').submit(function (e) {
        e.preventDefault();
        var formData = new FormData(this);

        $.ajax({
            url:"?",
            type:"post",
            dataType: "json",
            data: {form: formData, action: 'adicionarImagem'},
            success: function (data) {
                console.log(data)
            },
            error: function (data) {
                console.log(data);
            }
        });
    })
});