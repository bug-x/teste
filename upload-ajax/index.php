<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="main.css">

    <script src="./js/jquery-3.1.1.min.js"></script>

    <script>
        $(document).ready(function(){
            $("#btn_enviar").on('click',function(e) {
                
                e.preventDefault();
                var form = document.querySelector('#formulario');
                console.log(form);
                var formData = new FormData(form);

                $.ajax({
                    url: 'enviar.php',
                    type: 'post',
                    data: formData,
                    processData: false,
                    contentType: false,
                    xhr: function() { // Custom XMLHttpRequest
                        var myXhr = $.ajaxSettings.xhr();
                        if (myXhr.upload) { // Avalia se tem suporte a propriedade upload
                            myXhr.upload.addEventListener('progress', function(progress) {
                                var progresso = Math.ceil((100 * progress.loaded)  / progress.total);
                                console.log(progresso);
                                $('.bar-loading').css({
                                    'width': progresso + '%'
                                });
                                $('.v_progresso').html(progresso + '%');
                            }, false);
                        }
                        return myXhr;
                    },
                    success: function(data) {
                        console.log(data)
                    },
                    error: function(data){
                        console.log(data);
                    }
                
                });
            });
        });
    </script>

</head>
<body>

        <div class="container">
            <form id="formulario" action="" id="formVideo" enctype="multupartform-data">
                <input type="file" name="file" id="file_video"/>
                <label for="file_video">Envie seu arquivo</label>

                <button type="submit" id="btn_enviar">Enviar</button>
            </form>

            <div class="progressBar">
                <div class="bar-loading">
                    <span class="v_progresso"></span>
                </div>
            </div>
        </div>
</body>
</html>